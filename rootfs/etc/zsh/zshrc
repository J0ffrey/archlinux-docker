#!/bin/zsh
#------------------------------------------------------------------------------

HISTFILE="$HOME/.zsh_history"
HISTSIZE=1000
SAVEHIST=1000
ZPLUGINS='/usr/share/zsh/plugins'
ZCACHE="$HOME/.zsh_cache"

#------------------------------------------------------------------------------
# PLUGINS
#------------------------------------------------------------------------------

# powerline
# Statuslines and prompts
powerline-daemon --quiet
source '/usr/lib/python3.7/site-packages/powerline/bindings/zsh/powerline.zsh'

# zsh-syntax-highlighting
source "$ZPLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

# zsh-history-substring-search
source "$ZPLUGINS/zsh-history-substring-search/zsh-history-substring-search.zsh"

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down

#------------------------------------------------------------------------------

autoload -Uz compinit
compinit

setopt histignorealldups
setopt histignorespace

zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$ZCACHE"
zstyle ':completion:*' menu select
zstyle ':completion:*' auto-description '%d'
zstyle ':completion::complete:*' gain-privileges 1
# kill color: oh-my-zsh/lib/completion.zsh
zstyle ':completion:*:*:kill:*:processes' list-colors \
       '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
# case correction : oh-my-zsh/lib/completion.zsh
zstyle ':completion:*' matcher-list \
       'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' rehash true

function man() {
    # oh-my-zsh/plugins/colored-man-pages
    env \
        LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
        LESS_TERMCAP_md="$(printf "\e[1;31m")" \
        LESS_TERMCAP_me="$(printf "\e[0m")" \
        LESS_TERMCAP_se="$(printf "\e[0m")" \
        LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
        LESS_TERMCAP_ue="$(printf "\e[0m")" \
        LESS_TERMCAP_us="$(printf "\e[1;32m")" \
        PAGER="${commands[less]:-$PAGER}" \
        _NROFF_U=1 \
        man "$@"
}

alias ls='ls --color=auto'
alias ll='ls -lsh'
alias la='ls -lAsh'
alias vi='vim'
alias tree='tree -C'

export VISUAL='/usr/bin/vim'
