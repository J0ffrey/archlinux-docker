#!/bin/bash
IMG='joff/archlinux:latest'

case "$1" in
    -b|--build)
        docker build . -t "$IMG"
        ;;

    --host)
        docker run                          \
            --rm -t -i                      \
            --network host                  \
            --name arch_tmp                 \
            -v "$PWD"/home/:/home/chuck     \
            "$IMG"
        ;;

    *)
        docker run                          \
            --rm -t -i                      \
            --name arch_tmp                 \
            -v "$PWD"/home/:/home/chuck     \
            "$IMG"
        ;;
esac
