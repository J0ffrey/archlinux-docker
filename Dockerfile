from archlinux/base

ENV USER "chuck"
ENV HOME "/home/$USER"
ENV PKGDEST "$HOME/pkg"

# Language selection
ENV LANG "fr_FR.UTF-8"
RUN echo "$LANG UTF-8" > /etc/locale.gen; \
    locale-gen
ENV LANGUAGE fr_FR
ENV LC_ALL "$LANG"

# Adds custom mirrors for Pacman
ADD ./mirrorlist /etc/pacman.d/

# Install required packages
RUN pacman \
        --needed \
            findutils \
            gzip \
            pacman \
            sed \
            systemd \
            util-linux \
        -Syyu --noconfirm \
            base \
            base-devel \
            traceroute \
            cmake \
            tree \
            git \
            openssh \
            zsh \
            zsh-completions \
            zsh-syntax-highlighting \
            zsh-history-substring-search \
            powerline \
            vim \
            vim-seti \
            vim-jedi \
            vim-omnicppcomplete \
            vim-nerdtree \
            vim-syntastic \
            powerline-vim

# Uses all processor cores
RUN sed \
        -e '/-j2/s/#//' \
        -e "/-j2/s/2/$(nproc --all)/" \
        -e "s|#PKGDEST=/home/packages|PKGDEST=$PKGDEST|" \
        -i /etc/makepkg.conf

# Creates an administrator user for Makepkg
RUN useradd -m "$USER" -G wheel -s /bin/zsh; \
    echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# Built and install Yaourt
RUN sudo -u "$USER" -s mkdir /tmp/build "$PKGDEST"; \
    cd /tmp/build; \
    url='https://aur.archlinux.org'; \
    sudo -u "$USER" -s git clone "$url/package-query.git"; \
    sudo -u "$USER" -s git clone "$url/yaourt.git"; \
    cd ./package-query; \
    sudo -u "$USER" -s makepkg -sri --noconfirm  --asdeps; \
    cd ../yaourt; \
    sudo -u "$USER" -s makepkg -sri --noconfirm

# Build and install AUR packages
ADD ./autocomplpop.PKGBUILD /tmp/build/
RUN cd /tmp/build; \
    sudo -u "$USER" -s makepkg -p ./autocomplpop.PKGBUILD -sri --noconfirm

# Activates Pacman coloration
# Uses $PKGDEST as destination for packages built by yaourtrc
RUN sed \
        -e '/\[options\]/a ILoveCandy' \
        -e 's/#Color/Color/' \
        -i /etc/pacman.conf; \
    sed \
        -e 's/#EXPORT=0/EXPORT=1/' \
        -i /etc/yaourtrc

# Cleans image construction
RUN rm -rf /tmp/*

# Add custom configuration
ADD ./rootfs /

USER "$USER"
WORKDIR "$HOME"
ENTRYPOINT /bin/zsh
